import unittest
import pytest


def setUpModule():
    print('just do it!')


class TestMytest(unittest.TestCase):

    def test_dicts_equal(self):
        a = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        b = {'a': 1, 'b': 3, 'c': 3, 'd': 5}

        self.assertDictEqual(a, b)

    def test_lists_are_equal(self):
        a = [1, 2, 3, 4]
        b = [1, 2, 4, 5]

        self.assertSequenceEqual(a, b)

    def test_lists_are_equal_2(self):
        a = [{1: 1, 2: 2, 3: 4}, 2, 3, 4]
        b = [{1: 11, 2: 2, 3: 5}, 2, 4, 5]

        self.assertSequenceEqual(a, b)


class TestPytest:

    def test_dicts_equal(self):
        a = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        b = {'a': 1, 'b': 3, 'c': 3, 'd': 5}

        assert a == b


class TestBaseClassA(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('A')

    def test_true_is_true(self):
        self.assertTrue(True)


def test_me(somefixture):
    assert somefixture == 'car'


def test_is_skipped(skipper):
    assert True is False

