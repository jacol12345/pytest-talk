import pytest


def pytest_addoption(parser):
    parser.addoption('--hosts', type='string', action='append', dest='hosts')


def pytest_generate_tests(metafunc):
    if 'hosts' in metafunc.fixturenames:
        if metafunc.config.getoption('hosts'):
            metafunc.parametrize('host', metafunc.config.getoption('hosts'))


@pytest.fixture
def conn(host):
    conn = get_connection(host)
    yield conn
    conn.close()


@pytest.fixture
def skipper(request):
    if not request.config.getoption('--hosts'):
        pytest.mark.skip()
