# Pytest
Note:
+ not as sexy as other shows
+ questions to audience
+ AMA and interrupt me


 
# Why write unittests?
Note: Not a method of achieving reliability, but of accelerating development.



![Kent Beck and Erich Gamma](./img/beck-gamma.jpg)
Note:
+ Erich Gamma - Eclipse's Java Development Tools.
+ Kent Beck - Extreme Programming


## 1989 - Smalltalk
highly structured, object-oriented style unit testing library

Note:
Originally describedby Beck in 1989


## xUnit
Collective name for all frameworks originating from sUnit

Note:
* junit


## xUnit architecture
- runner <!-- .element: class="fragment" --> 
- case  <!-- .element: class="fragment" -->
- fixtures <!-- .element: class="fragment" -->
- suites  <!-- .element: class="fragment" -->
- execution <!-- .element: class="fragment" -->
- results formatter  <!-- .element: class="fragment" -->
- assertions <!-- .element: class="fragment" -->

Note:
- test runner - executable, runs tests, reports test results
- test case - all unit tests inherit from here
- test fixtures - set of preconditions to run a test
- test suites - set of tests that all share the same fixture
- test execution - steps taking place before and after individual test
- assertions = a function or macro that verifies the behaviour



## Python unit testing frameworks
##### (That you had a chance to hear about)
- unittest <!-- .element: class="fragment" -->
- pytest <!-- .element: class="fragment" -->
- nose <!-- .element: class="fragment" -->
- doctest <!-- .element: class="fragment" -->

Note:
* Unittest: "PyUnit"; Migrated directly from jUnit, popular
* Pytest: makes it easy to write small xUnit tests and scales to cover most complex cases.
* nose: a unittest extension
* doctest: interactive examples with results validation
 
focus on comparison between unittest and pytest



## can i use
|Unittest | Pytest
--- | ---
Python | 2.7, 3 | 2.6+, 3.3+, PyPy-2.3, Jython-2.5
Source | Python distribution | third-party app
License | GPL compatible | MIT

Note:
* unittest2 is a backport of new features to Pythons 2.4-2.6
* focus only on newest versions
* pytest can run unittest suites! no migration problem

GPL-compatible doesn’t mean that we’re distributing Python under the GPL.
All Python licenses, unlike the GPL, let you distribute a modified version without
making your changes open source. The GPL-compatible licenses make it possible to
combine Python with other software that is released under the GPL; the others don’t.

So what's the difference?



<!-- .slide: data-background-image="./img/same.gif" -->

Note:
* What's the difference?
* camelCase is not the main difference



## Run
```bash
$ python -m unittest test_module1 test_module2
$ python -m unittest test_module.TestClass
$ python -m unittest test_module.TestClass.test_method
```


## Run
```
$ pytest test_module1.py test_module2.py
$ pytest test_module.py::TestClass
$ pytest test_module.py::TestClass::test_method
$ pytest test_module.py::test_method
```


## Run
```
$ pytest -k http
$ pytest -k "not http"
$ pytest -k "http or quick"
$ pytest -k "http and slow"
```



## Markers
 
```
$ pytest --markers
@pytest.mark.slow: mark a test as slow execution test.
...
```


## Markers
```
$ pytest -m "not slow"
```


## Markers
```python
@pytest.mark.slow
def test_identity(self):
    assert True is True
```
 
Note: Just decorate test with a marker!


## Markers
```ini
# pytest.ini
[pytest]
markers =
    slow: mark a test as slow execution test.
```

Note: That's how markers get description



## Skipping tests
```python
@unittest.skipIf(...)
class XYTestCase(unittest.TestCase):

    @unittest.skip('x not ready')
    def test_x(self):
        ...

    @unittest.skipUnless(y_ready is not True)
    def test_y(self):
        ...

```
<!-- .element: class="fragment" -->

Note:
* @skipUnless - WTF?
* no default way of skipping module
* them camelCases...


## Skipping tests
```python
@pytest.mark.skipif(...)
class TestSkippedClass:

    @pytest.mark.skip(...)
    def test_function():
    ... 
```
<!-- .element: class="fragment" -->

Note:
* skip class or function/method
* for pytest class is just another organization level


## Skipping tests
```python
pytestmark = pytest.mark.skipif(...)
```
<!-- .element: class="fragment" -->

Note:
skip a module!


## Skipping tests
```python
minversion = pytest.mark.skipif(...)
```
<!-- .element: class="fragment" -->

```python
from test_module import skip_on_windows
```
<!-- .element: class="fragment" -->

```python
@minversion
@skip_on_windows
def test_function():  # skip function
    ...
```
<!-- .element: class="fragment" -->

Note:
* my own decorator
* marker can be imported from a different module
multiple decorators



## Expected failure
```python
@unittest.expectedFailure
def test_fail(self):
    self.assertTrue(False)
```
<!-- .element: class="fragment" -->
Note:
* cannot expect failure under condition


## Expected failure
```python
@pytest.mark.xfail
def test_fail(self):
    assert True is False
```
<!-- .element: class="fragment" -->


## Expected failure
```python
@pytest.mark.xfail(
    some_reason == True,
    reason='because of reason'
)
def test_fail_because_of_reasons(self):
    assert True is False
```
<!-- .element: class="fragment" -->



# Parametrization
```python
from x import even

class NumbersTest(unittest.TestCase):

    def test_even(self):
        values = [0, 1, 2, 3]
        results = [True, False, True, False]
        for val, res in zip(values, results):
            with self.subTest(val=val, res=res):
                self.assertEqual(even(val), res)
```
<!-- .element: class="fragment" -->

Note:
* subtest


## Parametrization
```python
from my_module import even

class NumbersTest(unittest.TestCase):
    
    @parametrized.expand([
        (0, True), (1, False), (2, True), (3, False),
    ])
    def test_even(self, value, result):
        self.assertEqual(even(value), result)
```

Note:
external library "parametrized"


## Parametrization
```python
from my_module import odd

@pytest.mark.parametrize('value,result', [
    (0, False), (1, True), (2, False), (3, True),
])
def test_odd(value, result):
    assert odd(value) == result
```

Note:
can parametrize class or module!


## Parametrization
```python
from my_module import odd

@pytest.mark.parametrize('value,result', [
    pytest.param(0, True, marks=pytest.mark.xfail),
])
def test_odd(value, result):
    assert odd(value) == result
```

Note: fail included!


## Parametrization
```python
@pytest.mark.parametrize('x', [0, 1])
@pytest.mark.parametrize('y', [2, 3])
def test_foo(x, y):
    pass
```

Note:
It would run x=0/y=2, x=0/y=3, x=1/y=2, x=1/y=3



## Debugging

```python
pytest --pdb
pytest -x --pdb
pytest --pdb --maxfail=3
```

Note:
pytest --pdb  # drop to PDB on each failure
pytest -x --pdb   # drop to PDB on first failure, then end test session
pytest --pdb --maxfail=3  # drop to PDB for first three failures

unittest:
Drops to pdb only when calling TestSuite.debug()
Why use test suites when unittest supports test discovery?



# What is wrong with assert*?

#### It's hard to remember all of them
<!-- .element: class="fragment" -->


assertEqual,
assertNotEqual,
assertTrue,
assertFalse,
assertIs,
assertIsNot,
assertIsNone,
assertIsNotNone,
assertIn,
assertNotIn,
assertIsInstance,
assertNotIsInstance,
assertAlmostEqual,
assertNotAlmostEqual,
assertGreater,
assertGreaterEqual,
assertLess,
assertLessEqual,
assertRegex,
assertNotRegex,
assertCountEqual,
assertMultiLineEqual,
assertSequenceEqual,
assertListEqual,
assertTupleEqual,
assertSetEqual,
assertDictEqual,
assertRaises,
assertRaisesRegex,
assertWarns,
assertWarnsRegex,
assertLogs

Note:
Long, isn't it?
So I went to check what was the assert for almost equal...


![Unittest page](./img/unit.png)



## Just assert
```python
def test_dicts_are_equal():
    a = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
    b = {'a': 1, 'b': 3, 'c': 3, 'd': 5}
    assert a == b
```

```
$ pytest
    
>       assert a == b
E       AssertionError: assert {'a': 1, 'b':...c': 3, 'd': 4} == {'a': 1, 'b': ...c': 3, 'd': 5}
E         Omitting 2 identical items, use -vv to show
E         Differing items:
E         {'b': 2} != {'b': 3}
E         {'d': 4} != {'d': 5}
E         Use -v to get the full diff

```
<!-- .element: class="fragment" -->


## Just assert
```
$ pytest --vv

>       assert a == b
E       AssertionError: assert {'a': 1, 'b':...c': 3, 'd': 4} == {'a': 1, 'b': ...c': 3, 'd': 5}
E         Common items:
E         {'a': 1, 'c': 3}
E         Differing items:
E         {'d': 4} != {'d': 5}
E         {'b': 2} != {'b': 3}
E         Full diff:
E         - {'a': 1, 'b': 2, 'c': 3, 'd': 4}
E         ?               ^               ^
E         + {'a': 1, 'b': 3, 'c': 3, 'd': 5}
E         ?               ^               ^
```
Note:
What if I'm not sure what to compare?
Output is very similiar but with pytest I save some boiler plate.

OK - it's not much boilerplate.



## Fixtures
```python
class TestMyFunc(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        pass
        
    def tearDown(self):
        pass
        
    @classmethod
    def tearDownClass(cls):
        pass
```
<!-- .element: class="fragment" -->

Note:
Remember about calling the parent method when overriding methods!


## Fixtures
```python
def setUpModule():
    pass

def tearDownModule():
    pass
   
```


## pytest fixtures
#### just a function
```python
@pytest.fixture
def smtp():
    import smtplib
    return smtplib.SMTP('smtp.gmail.com', 587, timeout=5)
```
<!-- .element: class="fragment" -->

Note:
* fixtures have explicit names and are activated by declaring their use from test functions, modules, classes or whole projects.
* fixtures are implemented in a modular manner, as each fixture name triggers a fixture function which can itself use other fixtures.
* fixture management scales from simple unit to complex functional testing, allowing to parametrize fixtures and tests
according to configuration and component options, or to re-use fixtures across class, module or whole test session scopes.


#### dependency injection
```python
def test_ehlo(smtp):
    response, msg = smtp.ehlo()
    assert response == 250
```
Note:
Fixtures: a prime example of dependency injection
Fixtures allow test functions to easily receive and work against specific pre-initialized application objects without
having to care about import/setup/cleanup details. It’s a prime example of dependency injection
where fixture functions take the role of the injector and test functions are the consumers of fixture objects.


#### fixture calls a fixture calls a fixture...
```python
@pytest.fixture
def person():
    return Person()
   
   
@pytest.fixture
def gun():
    return Gun()
    
    
@pytest.fixture
def bandit(person, gun):
    person.add_item(gun)
    return person
```


#### fixture levels
```python
@pytest.fixture(scope='session')
def db(): ...

@pytest.fixture(scope='module')
def smtp(): ...
    
@pytest.fixture(scope='class')
def disposable_index(): ...

@pytest.fixture(scope='function')
def db_transaction(): ...
```


#### fixture levels
```python
@pytest.mark.usefixtures('db', 'smtp', 'db_transaction')
class TestMyFunc:
    ...
```


#### fixture levels
```python
pytestmark = pytest.mark.usefixtures('db', 'smtp')
```

```ini
# pytest.ini
[pytest]
usefixtures = db smtp
```


#### autouse
```python
@pytest.fixture(scope='module')
def db():
    return DB()

class TestClass(object):
    @pytest.fixture(autouse=True)
    def transact(self, request, db):
        db.begin(request.function.__name__)
        yield
        db.rollback()

    def test_method1(self, db):
        assert db.intransaction == ['test_method1']

    def test_method2(self, db):
        assert db.intransaction == ['test_method2']
```
Note:
Here is how autouse fixtures work in other scopes:

+ autouse fixtures obey the scope= keyword-argument: if an autouse fixture has scope='session' it will only be run once, no matter where it is defined. scope='class'
means it will be run once per class, etc.
+ if an autouse fixture is defined in a test module, all its test functions automatically use it.
+ if an autouse fixture is defined in a conftest.py file then all tests in all test modules below its directory will invoke the fixture.
+ lastly, and please use that with care: if you define an autouse fixture in a plugin, it will be invoked for
all tests in all projects where the plugin is installed. This can be useful if a fixture only anyway works in the presence of certain settings
e. g. in the ini-file. Such a global fixture should always quickly determine if it should do any work and avoid otherwise expensive imports or computation.
Note that the above transact fixture may very well be a fixture that you want to make available in your project without having it generally active.
The canonical way to do that is to put the transact definition into a conftest.py file without using autouse:


#### fixture teardown
```python
@pytest.fixture(scope='session')
def db():
    db_.connect()
    
    yield db
    
    print('teardown db connection')
    db_.close()
```
Note:
if exception occurs in the setup code (before yield) the teardown code (after yield) will not be executed
Note solution: use alternative method: request.addfinalizer


#### fixture teardown
```python
@pytest.fixture(scope='session')
def db(request):
    db_.connect()
    
    def fin():
        print('teardown db connection')
        db_.close()
    request.addfinalizer(fin)
    
    return db
```
Note:
what is request?



## Request
```python
@pytest.fixture
def admin_get(request, client, admin):
    def get(params):
        endpoint = request.module.endpoint
        url = url_for(endpoint)
        return client.get(url, params=params, headers=admin.get_auth_headers)
    return get
```
Note:
can introspect test function, class or module



## Parametrize with CLI arguments
```python
@pytest.fixture
def connection(host):
    with get_connection(host) as conn:
        yield conn
```
<!-- .element: class="fragment" -->

```python
# conftest.py
def pytest_addoption(parser):
    parser.addoption('--hosts', type='string',
                     action='append', dest='hosts')
```
<!-- .element: class="fragment" -->

```python
def pytest_generate_tests(metafunc):
    hosts = metafunc.config.getoption('hosts')
    if 'host' in metafunc.fixturenames and hosts:
        metafunc.parametrize('host', hosts)
```
<!-- .element: class="fragment" -->


## Monkeypatching
```python
@pytest.fixture(autouse=True)
def no_requests(monkeypatch):
    monkeypatch.delattr('requests.sessions.Session.request')
```
Note: autouse seems a bit too powerful here



## XDist
#### Run your tests in parallel


## Just cool stuff
```bash
$ pytest --showlocals  # show local variables on failure
$ pytest --durations=5  # show 5 slowest tests
```



## Coverage up!

jacek.j.jankowski@gmail.com